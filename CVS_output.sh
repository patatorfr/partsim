#!/bin/bash

# Execution 'rtsim' with 4 arguments obligatoires: [architecture CPU / Taskset / temps de simulation / fichier de sortie]

	#OK
# 1) Définition des chemins d'arguments
rtsim="$(pwd)/build/rtsim/rtsim"
cpu_architecture_path="$(pwd)/simconf/systems/"
taskset_path="$(pwd)/simconf/tasksets/"
# PATH dossier du fichier 'output'
output_path="$(pwd)/"



	#OK
# 2) Execution rtsim
$rtsim $cpu_architecture_path$1 $taskset_path$2 $3 --t $4 




# 3) Sortie rtsim use pour créer un .csv plus lisible (pour omnetpp)
cat $output_path$4 | grep 'task' | grep 'ended' | grep -v 'Exec' > rtsim-output.txt || (echo "Fail"; exit 1) # fichier non transformé

rtsim_converted_file="rtsim_convert.csv" # fichier .csv lisible

# Supprime le fichier s'il existe déjà (évite d'écrire à la suite d'un préexistant)
if [ -f "$rtsim_converted_file" ]; then
    rm "$rtsim_converted_file"
fi

# Fonction : extract endtime/tâche/action d'un fichier .csv
extract_task_time_action() {
    line="$1"
    time="${line#\[Time:}"           # Remove "[Time:" from the beginning
    time="${time%%\]*}"              # Remove "]" and everything after it
    task="$(echo "$line" | grep -oE 'task_[0-9]+(_[0-9]+)?')"  # Extract 'task*'

    if echo "$line" | grep -q "scheduled"; then
        action="scheduled"
    elif echo "$line" | grep -q "descheduled"; then
        action="descheduled"
    else
        action="completed"
    fi

    echo "$time			$task		$action" >> "$rtsim_converted_file"
}

# Fonction : extrait les data sur l'architecture du processeur
extract_cpu_architecture() {
    #Variables locales stockant les diff' infos
    local name
    local numcpus
    local sched_type
    local task_repartition
    local freq
    # Lecture des infos
    while IFS= read -r line; do
        if echo "$line" | grep -q '^ *- name:'; then
            name=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *numcpus:'; then
            numcpus=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *scheduler'; then
            sched_type=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *task_placement:'; then
            task_repartition=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *base_freq:'; then
            freq=$(echo "$line" | awk -F': ' '{print $2}')       
        echo "$name			$numcpus			$sched_type				$task_repartition			$freq" >> "$rtsim_converted_file"
        fi
    done < "$tmpcpu"
}

# Fonction : extrait les info sur les tâches
extract_taskset() {
    #Variables locales stockant les diff' infos
    local name
    local iat
    local deadline
    local starting_cpu
    # Lecture des infos
    while IFS= read -r line; do
        if echo "$line" | grep -q '^ *- name:'; then
            name=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *iat:'; then
            iat=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *deadline'; then
            deadline=$(echo "$line" | awk -F': ' '{print $2}')
        elif echo "$line" | grep -q '^ *startcpu:'; then
            starting_cpu=$(echo "$line" | awk -F': ' '{print $2}')  
        echo "$name		$iat			$deadline				$starting_cpu" >> "$rtsim_converted_file"
        fi
    done < "$tmptask"
}

# Informations sur le type de CPU utilisé
echo "ISLAND_NAME		NUMCPUS			SCHEDULER_TYPE			TASK_PLACEMENT			FREQUENCY" > "$rtsim_converted_file" # Header
while IFS= read -r line; do
  if echo $line | grep -q 'power_models'; then # We don't want info on the power model
    break
  else 
    if echo "$line" | grep ' name:' | grep -q -v 'filename'; then # Si mot 'name' => début structure d'un CPU
      tmpcpu="tmpcpu_file.txt" # File tempo qui contiens les infos du CPU en question
      if [ -f "$tmpcpu" ]; then
        rm "$tmpcpu"
      fi
      echo "$line" >> "$tmpcpu" #Ecrit ligne 'name' dans 'tmp'
      while IFS= read -r line; do # Ecrit le reste des infos du CPU dans 'tmp'
        echo "$line" >> "$tmpcpu"
        if echo "$line" | grep -q 'speed_model'; then
          break
        fi
      done
      extract_cpu_architecture # Write CPU info dans 'rtsim_convert.csv' 
      # cat "tmp_file.txt" #ligne de debug
    fi
  fi
done < $cpu_architecture_path$1

# Informations sur les tâches tournant sur les CPU
echo "\nTASK_NAME		PERIOD(iat)		DEADLINE			STARTING_CPU" >> "$rtsim_converted_file" # Header
while IFS= read -r line; do
  if echo $line | grep -q 'resources'; then # We don't want info on the locks
    break
  else 
    if echo "$line" | grep ' name:' | grep -q -v ' the name'; then # Si mot 'name' => début structure d'une task
      tmptask="tmptask_file.txt" # File tempo qui contiens les infos du CPU en question
      if [ -f "$tmptask" ]; then
        rm "$tmptask"
      fi
      echo "$line" >> "$tmptask" #Ecrit ligne 'name' dans 'tmp'
      while IFS= read -r line; do # Ecrit le reste des infos du CPU dans 'tmp'
        echo "$line" >> "$tmptask"
        if echo "$line" | grep -q 'code:'; then
          break
        fi
      done
      extract_taskset # Write TASKS info dans 'rtsim_convert.csv' 
      # cat "tmptask_file.txt" #ligne de debug
    fi
  fi
done < $taskset_path$2
echo "\n" >> "$rtsim_converted_file"

# Sortie RTSim ligne par ligne [informations fin tasks]
echo "TIME			TASK			ACTION" >> "$rtsim_converted_file" # Header
while IFS= read -r line; do
    if echo "$line" | grep -q "\["; then
        extract_task_time_action "$line" # Fonction sur chaque ligne du fichier "rtsim-output.txt"
    fi
done < rtsim-output.txt







